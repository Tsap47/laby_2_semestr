#pragma once
#include <string>
#include <stdexcept>

enum Lang {
	JAVA = 0,
	CPP = 1,
	PHP = 2,
	PITON = 3
};

class CodeGenerator {
public:
	CodeGenerator() {}

	virtual ~CodeGenerator() {}

	std::string generateCode() {
		return "Result:\n" + someCodeRelatedThing();
	}
protected:
	virtual std::string someCodeRelatedThing() = 0;
};

class JavaCodeGenerator : public CodeGenerator {
public:
	JavaCodeGenerator() : CodeGenerator() {}

	virtual ~JavaCodeGenerator() {}

protected:
	std::string someCodeRelatedThing() override {
		return "Some java code";
	}
};

class CPPCodeGenerator : public CodeGenerator {
public:
	CPPCodeGenerator() : CodeGenerator() {}

	virtual ~CPPCodeGenerator() {}

protected:
	std::string someCodeRelatedThing() override {
		return "Some CPP code";
	}
};

class PHPCodeGenerator : public CodeGenerator {
public:
	PHPCodeGenerator() : CodeGenerator() {}

	virtual ~PHPCodeGenerator() {}

protected:
	std::string someCodeRelatedThing() override {
		return "Some php code";
	}
};

class PITONCodeGenerator : public CodeGenerator {
public:
	PITONCodeGenerator() : CodeGenerator() {}

	virtual ~PITONCodeGenerator() {}

protected:
	std::string someCodeRelatedThing() override {
		return "Some PITON code";
	}
};

CodeGenerator* codeFactory(enum Lang language) {
	switch (language) {
	case JAVA:return new JavaCodeGenerator();
	case CPP:return new CPPCodeGenerator();
	case PHP:return new PHPCodeGenerator();
	case PITON:return new PITONCodeGenerator();
	}
	throw std::logic_error("Bad language");
}