﻿#include <iostream>
#include "Header.hpp"

int main() {
	CodeGenerator* code = codeFactory(PITON);
	std::cout << code->generateCode() << std::endl;

	delete code;
	return 0;
}