﻿#include <iostream>
#include <memory>
#include <vector>
#include <map>
using namespace std;
class teacher;
class student;

class student {
public:
    student(const string& name) : m_studentName(name) { }
    ~student() { }
    const weak_ptr<teacher> getTeacher() { return m_teacher; }
    const string& getName() const { return m_studentName; }
    friend void addStudentToTeacher(shared_ptr<teacher>& Teacher, shared_ptr<student>& Student);
private:
    string m_studentName;
    weak_ptr<teacher> m_teacher;
};

class teacher {
public:
    teacher(const string& name) : m_teacherName(name) { }
    ~teacher() { }
    void addStudent(shared_ptr<student>& Student) {
        m_students.push_back(Student);
    }
    void getAllStudents() {
        for (auto const& student : m_students)
            cout << student->getName() << endl;
    }
    const string& getName() const { return m_teacherName; }
    friend void addStudentToTeacher(shared_ptr<teacher>& Teacher, shared_ptr<student>& Student);
private:
    string m_teacherName;
    vector<shared_ptr<student>> m_students;
};

void addStudentToTeacher(std::shared_ptr<teacher>& Teacher, shared_ptr<student>& Student)
{
    Student->m_teacher = Teacher;
    Teacher->addStudent(Student);
}

int main() {
    setlocale(LC_ALL, "Russian");
    auto Teacher_1{ make_shared<teacher>("Учитель_1") };
    auto Teacher_2{ make_shared<teacher>("Учитель_2") };
    auto Student_1{ make_shared<student>("Студент_1") };
    auto Student_2{ make_shared<student>("Студент_2") };
    auto Student_3{ make_shared<student>("Владимир Белочкин") };
    addStudentToTeacher(Teacher_1, Student_1);
    addStudentToTeacher(Teacher_1, Student_2);
    addStudentToTeacher(Teacher_2, Student_3);

    Teacher_1->getAllStudents();
    cout << "------------"<< endl;
    Teacher_2->getAllStudents();

    return 0;
}