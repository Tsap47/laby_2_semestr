﻿#include <iostream>
#include <fstream>
//#include "src/Functions.cpp"
#include "inc/FunctionsHeader.hpp"

#include "inc/mathutils/matrix.hpp"

int main() {
	BMP bmp_Image(540, 960);
	bmp_Image.Read();
	//bmp_Image.Pixels();
	//bmp_Image.DarkFilter();
	bmp_Image.Rotate(acos(-1) / 4);
	bmp_Image.Correct();
	bmp_Image.Write();

	//bmp_Image.Clean();
	//bmp_Image.Rotate(acos(-1)/4);
	return 0;
}


