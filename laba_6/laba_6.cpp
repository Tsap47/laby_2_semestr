﻿/*Вариант 5.
Подобрать структуру для хранения данных, над которой преимущественно будут осуществляться операции поиска и добавления.
Минимально – удаления.
*/

#include <iostream>
#include <algorithm>
#include <cmath>
#include <conio.h>
#include <chrono>
#include <cstdlib>
#include <ctime>

using namespace std;
int N, B, A = 0;
class Timer
{
private:
	// Псевдонимы типов используются для удобного доступа к вложенным типам
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

struct T_List
{
	T_List* next;
	//std::string surname;
	int age;
};

int Search(T_List* head, int r) { //поиск для односвязного списка
	T_List* p = head->next;
	while (p != nullptr)
	{
		if (p->age == r)
			return true;
		p = p->next;
	}
	return false;

}
void Add(T_List* head, int age)
{
	T_List* p = new T_List;
	p->age = age;

	p->next = head->next;
	head->next = p;
}

void Print(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->age << std::endl;
		p = p->next;
	}
}

void Delete(T_List* head, int i)
{
	T_List* tmp;
	T_List* p = head;
	while (p->next != nullptr)
	{
		if (p->next->age  == i)
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
		}
		else
			p = p->next;
	}
}

void Clear(T_List* head)
{
	T_List* tmp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}

void Duplicate(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		if (p->age % 2 == 1)
		{
			T_List* q = new T_List;
			q->next = p->next;
			p->next = q;
			q->age = p->age;
			p = p->next;
		}
		p = p->next;
	}
}

void Swapsort(T_List* head)
{
	//for(int i=0;i<n-1;i++)
	//	for(int j=i+1;j<n;j++)

	T_List* p = head->next;
	while (p->next->next != nullptr)
	{
		T_List* q = p->next;
		while (q->next != nullptr)
		{
			if (p->age > q->age)
				std::swap(p->age, q->age);
			q = q->next;
		}
		p = p->next;
	}
}


struct Elem
{
	int data;       // 
	//    
	Elem* left;
	Elem* right;
	Elem* parent;
};

Elem* MAKE(int data, Elem* p)
{
	Elem* q = new Elem;         //    
	q->data = data;
	q->left = nullptr;
	q->right = nullptr;
	q->parent = p;
	return q;
}
void ADD(int data, Elem*& root)
{
	if (root == nullptr) {
		root = MAKE(data, nullptr);
		return;
	}
	Elem* v = root;
	while ((data < v->data && v->left != nullptr) || (data > v->data && v->right != nullptr))
		if (data < v->data)
			v = v->left;
		else
			v = v->right;
	if (data == v->data)
		return;
	Elem* u = MAKE(data, v);
	if (data < v->data)
		v->left = u;
	else
		v->right = u;
}

//  
void PASS(Elem* v)
{
	if (v == nullptr)
		return;
	// 

	PASS(v->left);
	// 
	std::cout << v->data << std::endl;

	PASS(v->right);

	// 
}
bool SEARCHBOOL(int data, Elem* v) // v - ,    
{
	if (v == nullptr)
		return false;
	if (v->data == data)
		return true;
	if (data < v->data)
		return SEARCHBOOL(data, v->left);
	else
		return SEARCHBOOL(data, v->right);
}

Elem* SEARCH(int data, Elem* v) // v - ,    
{
	if (v == nullptr)
		return v;
	if (v->data == data)
		return v;
	if (data < v->data)
		return SEARCH(data, v->left);
	else
		return SEARCH(data, v->right);
}

void DELETE(int data, Elem*& root)
{
	//   .  
	Elem* u = SEARCH(data, root);
	if (u == nullptr)
		return;

	//   ( )
	if (u->left == nullptr && u->right == nullptr && u == root)
	{
		delete root;
		root = nullptr;
		return;
	}

	//   
	if (u->left == nullptr && u->right != nullptr && u == root) // TODO  u==root
	{
		Elem* t = u->right;
		while (t->left != nullptr)
			t = t->left;
		u->data = t->data;
		u = t;
	}

	//   
	if (u->left != nullptr && u->right == nullptr && u == root) // TODO  u==root
	{
		Elem* t = u->left;
		while (t->right != nullptr)
			t = t->right;
		u->data = t->data;
		u = t;
	}

	//    
	if (u->left != nullptr && u->right != nullptr)
	{
		Elem* t = u->right;
		while (t->left != nullptr)
			t = t->left;
		u->data = t->data;
		u = t;
	}
	Elem* t;
	if (u->left == nullptr)
		t = u->right;
	else
		t = u->left;
	if (u->parent->left == u)
		u->parent->left = t;
	else
		u->parent->right = t;
	if (t != nullptr)
		t->parent = u->parent;
	delete u;
}



int main()
{
	int g, r, t, i;

	setlocale(LC_ALL, "Russian");
	Elem* root = nullptr;
	T_List* head = new T_List;
	head->next = nullptr;

	cout << "Привет. Это программа считает время осуществение операций поиска," << endl << "добавления и удаления элементов в бинарном дереве и в списке." << endl << "Сколько чисел вы хотите в последовательности? ";
	cin >> B;
	//cout << "Введите последовательность из " << B << " символов: " << endl;
	//cin >> N;
	srand(time(0));
	
	while (A < B) {
		N = rand() % 10000;
		ADD(N, root);
		Add(head, N);
		A++;
	}
	A = 0;
	system("cls");
	cout << "===========" << endl;
	cout << "Создан список:" << endl;
	Print(head);
	cout << "===========" << endl;
	cout << "Создано бинарное дерево:" << endl;
	PASS(root);
	cout << "===========" << endl;
	cout << "Что вы хотите сделать с последовательностью? Введите " << endl << "1 - Найти число" << endl << "2 - Добавить число" << endl << "3 - Удалить число" << endl;
	cin >> g;
	if (g == 1) {
		cout << "Какое число вы хотите найти?" << endl;
		cin >> i;
		system("cls");
		Timer B;
		double timeSearch1;
		if (SEARCHBOOL(i, root) == true) {
			timeSearch1 = B.elapsed();
			cout << "Время нахождения числа " << i << " в бинарное дерево составило: " << timeSearch1 << endl;
		}
		else {
			timeSearch1 = B.elapsed();
			cout << "Таких чисел нет" << timeSearch1;
		}
		Timer N;
		double timeSearch2;

		if (Search(head, i) == true) {
			timeSearch2 = N.elapsed();
			cout << "Время нахождения числа " << i << " в списке составило: " << timeSearch2 << endl;
		}
		else {
			timeSearch2 = N.elapsed();
			cout << "Таких чисел нет" << timeSearch2;
		}
		cout << "===========" << endl;
		if (timeSearch1 < timeSearch2) {
			cout << "В бинарное дерево число нашлось быстрее" << endl;
		}
		else if (timeSearch1 > timeSearch2) {
			cout << "В списке число нашлось быстрее" << endl;
		}
		else {
			cout << "В списке и в бинарном дереве число нашлось за одно и тоже время - " << timeSearch1 << endl;
		}
	}
	
	else if (g == 2) {
		cout << "Какое число вы хотите добавить?" << endl;
		cin >> r;
		system("cls");
		Timer Q;
		double timeSearch3;
		ADD(r, root);
		timeSearch3 = Q.elapsed();
		cout << "Время добавления числа " << r << " в бинарное дерево составило: " << timeSearch3 << endl;
		Timer R;
		double timeSearch4;
		Add(head, r);
		timeSearch4 = R.elapsed();
		cout << "Время добавления числа " << r << " в список: " << timeSearch4 << endl;
		cout << "===========" << endl;
		cout << "Вот новый список: " << endl;
		Print(head);
		cout << "===========" << endl;
		cout << "Вот новое бинарное дерево: " << endl;
		PASS(root);
		cout << "===========" << endl;
		if (timeSearch3 < timeSearch4) {
			cout << "В бинарное дерево число занеслось быстрее" << endl;
		}
		else if (timeSearch3 > timeSearch4) {
			cout << "В список число занеслось быстрее" << endl;
		}
		else {
			cout << "В список и бинарное дерево число занеслось за одно и тоже время - " << timeSearch3 << endl;
		}
		//cout << "Время добавления числа " << r << " в список составило: " << Q.elapsed() << endl;
	   // cout << "Время добавления числа " << r << " в бинарное дерево: " << R.elapsed() << endl;
	}
	else if (g == 3) {
		cout << "Какое число вы хотите удалить?" << endl;
		cin >> t;
		system("cls");
		Timer E;
		double timeSearch5;
		DELETE(t, root);
		timeSearch5 = E.elapsed();
		cout << "Время удаления числа " << t << " в бинарном дереве составило: " << timeSearch5 << endl;
		Timer H;
		double timeSearch6;
		Delete(head, t);
		timeSearch6 = H.elapsed();
		cout << "Время удаления числа " << t << " в списке составило: " << timeSearch6 << endl;
		cout << "===========" << endl;
		cout << "Вот новый список: " << endl;
		Print(head);
		cout << "===========" << endl;
		cout << "Вот новое бинарное дерево: " << endl;
		PASS(root);
		cout << "===========" << endl;
		if (timeSearch5 < timeSearch6) {
			cout << "В бинарное дереве число удалилось быстрее" << endl;
		}
		else if (timeSearch5 > timeSearch6) {
			cout << "В списке число удалилось быстрее" << endl;
		}
		else {
			cout << "В списке и в бинарном дереве число удалилось за одно и тоже время - " << timeSearch5 << endl;
		}
		// Delete(t);
	}
	else {
		cout << "Вы всё сделали неправильно(";
	}
	system("pause");

	// std::cout << "Hello World!\n";
}

